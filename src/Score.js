import React, { Component } from 'react';
import './Score.css';

class Score extends Component {
  constructor(props){
    super(props);
    console.log(props);  
  }

  percentScore(x){
    return 100 - ((x / 50) * 100)
  }


  render() {
    return (
      <div className="ScoreCard">
        <ul>
          <li>Find <strong>{this.props.now}</strong> </li>
          <li className="Status"> {50 - this.props.count} / 50</li>
          <li className="Percent"> {this.percentScore(this.props.errors)}%</li>
        </ul>
        <span className="Refresh">
        <i onClick={this.props.refresh} className="fa fa-refresh" aria-hidden="true"></i>
        </span>
      </div>
    );
  }
}

export default Score;