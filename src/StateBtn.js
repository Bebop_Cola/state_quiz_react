import React, { Component } from 'react';
import ConfigStates from './configs/states';

class StateBtn extends Component {
  constructor(props){
    super(props);
    this.state = {
      id: props.id,
      click: props.click,
      draw: props.draw,
      class: '',
      }
    
  }
  handleChange(event) {
    this.setState({ class: this.props.class });
  }
  componentDidMount () {
    this.setState({class: this.props.class});
  }
  componentWillReceiveProps(newProps) {
    if (this.state.class !== newProps.class) {
      this.setState({class: newProps.class});
    }
  }
  render() {
    return (
      <path id={this.state.id} onChange={this.handleChange} onClick={this.state.click} d={this.state.draw} className={this.state.class} />
    );
  }
}

export default StateBtn;