import React, { Component } from 'react';
import './App.css';
import Map from './Map';
import ConfigStates from './configs/states';

class App extends Component {
  render() {
    return (
      <div className="App">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossOrigin="anonymous" />
        <Map>
        </Map>
        
      </div>
    );
  }
}

export default App;
